import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.application.Application;
import javafx.geometry.Insets;
//import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
//import javafx.scene.control.TextArea;
//import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
//import javafx.scene.paint.Color;
//import javafx.scene.paint.LinearGradient;
//import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HappyMovements extends Application {
    public void start(Stage stage) throws Exception {

        String getDate = MyCalendar.getFinalDate();
        HBox root = new HBox();
        VBox vb1 = new VBox();
        VBox vb2 = new VBox();
        // spacing
        vb1.setSpacing(50);
        root.setSpacing(550);
        vb2.setSpacing(80);
        Image backgroundImage = new Image("CalenderPageImage/diarycontent.png");

        // Create an ImageView and set the background image as its content
        ImageView backgroundImageView = new ImageView(backgroundImage);
        root.setStyle("-fx-background-image: url('CalenderPageImage/diarycontent.png'); " +
                "-fx-background-size: cover;");

        Image image1 = new Image("Icon/achievement_button_icon.png");
        Image image2 = new Image("Icon/happy_button_icon.png");
        Image image3 = new Image("Icon/angry_button_icon.png");
        Image image4 = new Image("Icon/Notebook_icon.png");
        ImageView achIcon = new ImageView(image1);
        ImageView happyIcon = new ImageView(image2);
        ImageView angIcon = new ImageView(image3);
        ImageView dailyNotesIcon = new ImageView(image4);
        // button icon
        double heightImg = 50;
        double weidthImg = 50;
        achIcon.setFitHeight(heightImg);
        achIcon.setFitWidth(weidthImg);

        happyIcon.setFitHeight(heightImg);
        happyIcon.setFitWidth(weidthImg);

        angIcon.setFitHeight(heightImg);
        angIcon.setFitWidth(weidthImg);

        dailyNotesIcon.setFitHeight(heightImg);
        dailyNotesIcon.setFitWidth(weidthImg);
        // buttons in vbox

        Button achivement = new Button("", achIcon);
        Button happy = new Button("", happyIcon);
        Button ang = new Button("", angIcon);
        Button dailyNotes = new Button("", dailyNotesIcon);
           Button back=new Button("Back");


         back.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 25));
        back.setStyle("-fx-background-color: #952323; -fx-text-fill: white; -fx-padding: 10 20; -fx-background-radius: 10;");
        back.setOnAction(event -> {
            Memory dc = new Memory();
            try{
                dc.start(stage);
            }
            catch(Exception e1){
                
            }
        });
        /*
         * Stop[] stops = new Stop[] {
         * new Stop(0, Color.WHITE),
         * new Stop(1,Color.BLACK)
         * };
         * //gradient color for button
         * LinearGradient gradient = new LinearGradient(0, 0, 0, 1, true, null, stops);
         */
        happy.setStyle(
                "-fx-background-color: linear-gradient(to right, white,rgb(244, 206, 20));-fx-font-size:35px");
        ang.setStyle(
                "-fx-background-color: linear-gradient(to right, white, rgb(216, 0, 50));-fx-font-size:35px");
        dailyNotes.setStyle(
                "-fx-background-color: linear-gradient(to right, white, rgb(25, 38, 85));-fx-font-size:35px");
        achivement.setStyle(
                "-fx-background-color: linear-gradient(to right, white, rgb(167, 211, 151));-fx-font-size:35px");
        // margin and padding in vbox elements/* 
        vb1.setMargin(achivement, new Insets(10, 0, 0, 5));
        vb1.setMargin(happy, new Insets(10, 0, 0, 5));
        vb1.setMargin(ang, new Insets(10, 0, 0, 5));
        vb1.setMargin(dailyNotes, new Insets(10, 0, 0, 5));
        vb1.setMargin(back, new Insets(10, 0, 0, 5));
        vb1.setPadding(new Insets(10));
        

        String descriptions = "";
      

        String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/Diary";
        String username = "root";
        String password = "Warmachine@17";

        try {
            Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
            String sql = "SELECT description FROM entry WHERE date="+ "'" + getDate + "'"+" AND id=1";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                //description = resultSet.getString("description");
                String description = resultSet.getString("description");
                descriptions += description + "\n";
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    



        vb1.getChildren().addAll(achivement, happy, ang, dailyNotes,back);
        //MyCalendar cal = new MyCalendar();

        getDate = MyCalendar.getFinalDate();

        String title = "Happy " + getDate;

        Label heading = new Label(title);
        heading.setStyle(
                "-fx-font-size:50; -fx-text-fill: rgb(244, 206, 20);-fx-font-family: 'Lucida Handwriting';-fx-font-weight:700");
        String str =descriptions;
        String wrapText = wrapText(str, 8);
        Text tx = new Text(wrapText);
        tx.setStyle(
                "-fx-font-size:35px;");

       vb2.getChildren().addAll(heading, tx);

        // margin and padding for vb2 box
        /*vb2.setMargin(tx, new Insets(0, 0, 0, 40));

        // paddind , margin and spacing for hBox Elements
        root.setMargin(vb1, new Insets(100, 50, 0, 60));
        root.setMargin(vb2, new Insets(0, 0, 0, 220));*/

        // onclick events
        ang.setOnMouseClicked(e -> {
            System.out.println("clicked");
            AngryMovements angryMovementsPage = new AngryMovements();
            try {
                angryMovementsPage.start(stage);
            } catch (Exception e1) {

            }

        });

        dailyNotes.setOnMouseClicked(e -> {
            DailyNotes dn = new DailyNotes();
            try {
                dn.start(stage);

            } catch (Exception e1) {

            }

        });
        achivement.setOnMouseClicked(e -> {
            Achivement achivementPage = new Achivement();
            try {
                achivementPage.start(stage);

            } catch (Exception e1) {

            }

        });

        root.getChildren().addAll(vb1, vb2);
        Scene scene = new Scene(root,2000,1000);
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.show();

    }

    private String wrapText(String input, int wordsPerLine) {
        String[] words = input.split(" ");
        StringBuilder result = new StringBuilder();
        int wordCount = 0;

        for (String word : words) {
            result.append(word).append(" ");
            wordCount++;

            if (wordCount >= wordsPerLine) {
                result.append("\n");
                wordCount = 0;
            }
        }

        return result.toString();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("hello world");
        launch(args);
    }
}
