//package com.diary;

import javafx.animation.ScaleTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class Memory extends Application {

    private VBox optionButtons;
    private boolean optionsVisible = false;
    Stage primarStage;

    public static void main(String[] args)throws Exception {
        launch(args);
    }
    

    @Override
    public void start(Stage primaryStage)throws Exception {
        this.primarStage=primaryStage;
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);
        root.setStyle("-fx-background-image: url('HomePageImage/background.png');" +
                "-fx-background-size: cover; -fx-background-repeat: no-repeat;");
        root.setPadding(new Insets(20));
        root.setMaxWidth(400);

        Image userImage = new Image(getClass().getResource("HomePageImage/user_image.png").toExternalForm());
        ImageView userImageView = new ImageView(userImage);
        userImageView.setFitWidth(250);
        userImageView.setFitHeight(250);
        userImageView.setEffect(new DropShadow(10, Color.BLACK));

        GridPane appTable = new GridPane();
        appTable.setHgap(10);
        appTable.setAlignment(Pos.CENTER);

        Button aboutUsButton = createStyledButton("About Us", "HomePageImage/icons8-about-50.png");
        Button calendarButton = createStyledButton("Calendar", "HomePageImage/calendar_927736.png");
        Button toDoListButton = createStyledButton("To-Do List", "HomePageImage/to-do-list.png");
        Button insertButton = createStyledButton("Insert", "HomePageImage/icons8-insert-page-100.png");

        aboutUsButton.setOnAction(event -> openAboutUsDialog());
        calendarButton.setOnAction(event -> {
            DairyCalendar dairyCalendar= new DairyCalendar();
            try {
                dairyCalendar.start(primaryStage);
            } catch (Exception e) {
               
            }

        });
        toDoListButton.setOnAction(event -> {
            ToDoList todolistPage = new ToDoList();
            try{
            todolistPage.start(primaryStage);
            }
            catch(Exception e1){}

        });
        insertButton.setOnAction(event -> toggleOptionButtons());

        appTable.add(aboutUsButton, 0, 2);
        appTable.add(calendarButton, 1, 2);
        appTable.add(toDoListButton, 2, 2);
        appTable.add(insertButton, 3, 2);

        Text userName = createText(" User", 24, FontWeight.BOLD, Color.rgb(28, 101, 150));
        Text quote = createText("\"Each day is a blank canvas on the Memory Palette of life. " +
                "Fill it with vibrant strokes of experiences and colorful memories\n\t\t\t that make your story a masterpiece.\"",
                20, FontWeight.NORMAL, Color.rgb(46, 129, 184));

        optionButtons = createOptionButtons();

        root.getChildren().addAll(appTable, userImageView, userName, quote, optionButtons);

        Scene scene = new Scene(root,2000,1000);
        
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        
        primaryStage.setTitle("Memory Palette");
        primaryStage.show();
    }

    private Button createStyledButton(String text, String imagePath) {
        Image iconImage = new Image(getClass().getResource(imagePath).toExternalForm());
        ImageView icon = new ImageView(iconImage);
        icon.setFitWidth(40);
        icon.setFitHeight(40);

        Button button = new Button(text, icon);
        button.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 14));
        button.setStyle("-fx-background-color: #3498db; -fx-text-fill: white; -fx-padding: 10 20; -fx-background-radius: 10;");
        button.setOnMouseEntered(event -> scaleButton(button, 1.1));
        button.setOnMouseExited(event -> scaleButton(button, 1.0));
        return button;
    }

    private Text createText(String text, int fontSize, FontWeight fontWeight, Color textColor) {
        Text textNode = new Text(text);
        textNode.setFont(Font.font("Monotype Corsiva", fontWeight, fontSize));
        textNode.setFill(textColor);
        return textNode;
    }

    private void openAboutUsDialog() {
       
        Stage aboutUsStage = new Stage();
        aboutUsStage.initModality(Modality.APPLICATION_MODAL);
        aboutUsStage.initStyle(StageStyle.UTILITY);
        aboutUsStage.setTitle("About Us");

        VBox aboutUsLayout = new VBox(40);
        aboutUsLayout.setAlignment(Pos.CENTER);
        aboutUsLayout.setPadding(new Insets(60));

        Text aboutUsText = createText("About Us:\n" +
                        "Hello we are HIGH FIVE,\nAtharva Bibave,\nRohan Misal,\nRohan Polekar,\nApurva Repal,\n Shivani Kachare,\n\nWe are students of core2web."
                        + "Here we're working on our knowledge development and gaining some realtime company project experience."
                        + "\nwhich is named as super-X 2.0 program."
                        + "We created a Memory Palette application for you so that you can create"
                        + "\na record of your memorable moments. We ensure you that your memories are safe here."
                        + "\nFeel free to write your heart's desire."
                        + "\n"
                        + "\n        Thank you for Joining!      ",
                20, FontWeight.NORMAL, Color.BLACK);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(aboutUsText);
        scrollPane.setFitToWidth(true); 

        aboutUsLayout.getChildren().add(scrollPane);

        Scene aboutUsScene = new Scene(aboutUsLayout, 800, 400);
        aboutUsStage.setScene(aboutUsScene);
        aboutUsStage.showAndWait();
    }

   

    private void scaleButton(Button button, double scale) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(200), button);
        scaleTransition.setToX(scale);
        scaleTransition.setToY(scale);
        scaleTransition.setCycleCount(1);
        scaleTransition.setAutoReverse(false);
        scaleTransition.play();
    }

    private VBox createOptionButtons() {
        VBox options = new VBox(20);
        options.setAlignment(Pos.CENTER);
        options.setPadding(new Insets(30));
        options.setVisible(false);

        Button option1 = createOptionButton("Achievements");
        Button option2 = createOptionButton("Happy");
        Button option3 = createOptionButton("Angry");
        Button option4 = createOptionButton("Daily Notes");

        option1.setOnAction(e->{
            AchievementText achivement=new AchievementText();
            try {
                achivement.start(primarStage);
            } catch (Exception e1) {
                
            }
        });
        option2.setOnAction(e->{
            HappyText happyText = new HappyText();
            try{
            happyText.start(primarStage);
            }
            catch(Exception e1){

            }
        });
        option3.setOnAction(e->{
            AngryText angryText = new AngryText();
            try{
                angryText.start(primarStage);
            }
            catch(Exception e1){

            }
        });
        option4.setOnAction(e->{
            Daily_Notes_Icon dailyText = new Daily_Notes_Icon();
            try{
                dailyText.start(primarStage);
            }
            catch(Exception e1){

            }
        });

        options.getChildren().addAll(option1, option2, option3, option4);

        return options;
    }

    private Button createOptionButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 18));
        button.setStyle("-fx-background-color: #27ae60; -fx-text-fill: white; -fx-padding: 10 20; -fx-background-radius: 10;");
        button.setOnMouseEntered(event -> scaleButton(button, 1.1));
        button.setOnMouseExited(event -> scaleButton(button, 1.0));
        return button;
    }

    private void toggleOptionButtons() {
        optionsVisible = !optionsVisible;
        optionButtons.setVisible(optionsVisible);
    }
}
