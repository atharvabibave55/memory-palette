import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Login extends Application {
    public static Stage primStage;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primStage=primaryStage;
        primaryStage.setTitle("Memory Palette - Login");

        GridPane grid = createLoginForm();

        Image backgroundImage = new Image("back.jpg");
        BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);

        grid.setBackground(new Background(background));

        Scene scene = new Scene(grid);
        primaryStage.setMaximized(true);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private GridPane createLoginForm() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(50, 50, 50, 50));

        Label titleLabel = new Label("Memory Palette");
        titleLabel.setFont(Font.font("Monotype Corsiva", 60));
        titleLabel.setTextFill(Color.web("#0047AB"));

        Label usernameLabel = new Label("Username");
        TextField usernameField = new TextField();
        usernameField.setPromptText("Enter Username");

        Label passwordLabel = new Label("Password");
        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Enter Password");

        Button loginButton = new Button("Log In");
        loginButton.setStyle("-fx-background-color: #125688; -fx-text-fill: white; -fx-font-weight: bold;");
        loginButton.setMinWidth(200);

        loginButton.setOnMouseEntered(event -> loginButton.setStyle("-fx-background-color: #0D456C; -fx-text-fill: white; -fx-font-weight: bold;"));
        loginButton.setOnMouseExited(event -> loginButton.setStyle("-fx-background-color: #125688; -fx-text-fill: white; -fx-font-weight: bold;"));
        loginButton.setOnMousePressed(event -> loginButton.setStyle("-fx-background-color: #0B3556; -fx-text-fill: white; -fx-font-weight: bold;"));

        Hyperlink createAccountLink = new Hyperlink("Not a user? Create Account");
        createAccountLink.setTextFill(Color.web("#0096FF"));
        createAccountLink.setOnAction(e -> handleCreateAccount());

        grid.add(titleLabel, 0, 0, 2, 1);
        grid.add(usernameLabel, 0, 1);
        grid.add(usernameField, 1, 1);
        grid.add(passwordLabel, 0, 2);
        grid.add(passwordField, 1, 2);
        grid.add(loginButton, 1, 3);
        grid.add(createAccountLink, 1, 4);

        loginButton.setOnAction(e->{
            Memory m= new Memory();
            try{
                m.start(primStage);
            }
            catch(Exception  e1){

            }
        });

        return grid;
    }

    private void handleCreateAccount() {
        System.out.println("Navigate to the registration page or perform other actions");
    }
}
