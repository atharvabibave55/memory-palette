import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.effect.DropShadow;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.util.Calendar;
import java.util.GregorianCalendar;

class MyCalendar extends BorderPane {
    Calendar currentMonth;
    private BorderPane mybox = new BorderPane();
    ComboBox<String> selectMonth;
    ComboBox<Integer> selectYear;
    HBox hbox;
    Button submitbtn;
    String monthString;
    String yearString;
    GridPane gpBody;
    public static String finalDate;
    Alert alert;

    BorderPane getMyBox() {
        mybox = drawCalendar(mybox);
        return mybox;
    }

    MyCalendar() {
        currentMonth = new GregorianCalendar();
        currentMonth.set(Calendar.DAY_OF_MONTH, 1);

    }

    private BorderPane drawCalendar(BorderPane mybox) {

        mybox = drawHeader(mybox);
        mybox = drawBody(mybox);
        mybox=drawFooter(mybox);
        return mybox;
    }
    private BorderPane drawFooter(BorderPane mybox) {

        Button back = new Button();
        back.setText("Back");
        back.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 25));

        back.getStyleClass().add("my-btn");
        // back.setPadding(new Insets(10, 10, 10, 1));
        mybox.setBottom(back);
        System.out.println(back.getAlignment());
        // back.setAlignment(Pos.CENTER);
        back.setAlignment(Pos.CENTER);
        System.out.println(back.getAlignment());
      
        return mybox;
    }
    private BorderPane drawHeader(BorderPane mybox) {

        hbox = new HBox(100);

        submitbtn = new Button();
        submitbtn.setPrefSize(100, 5);
        submitbtn.setText("Submit");
        DropShadow shadow = new DropShadow();
        shadow.setColor(javafx.scene.paint.Color.GRAY);
        shadow.setRadius(5);
        shadow.setOffsetX(5);
        shadow.setOffsetY(5);

        // Apply the shadow effect to the button
        submitbtn.setEffect(shadow);

        submitbtn.getStyleClass().add("my-btn1");

        selectMonth = new ComboBox<>();
        selectMonth.getItems().addAll("January", "February", "March", "April", "May", "June", "July", "August",
                "September",
                "October", "November", "December");
        selectMonth.setPromptText("Select Month");

        selectYear = new ComboBox<>();
        selectYear.getItems().addAll(new Integer(2000), new Integer(2001), new Integer(2002), new Integer(2003),
                new Integer(2004), new Integer(2005), new Integer(2006), new Integer(2006), new Integer(2008),
                new Integer(2009), new Integer(2010), new Integer(2011), new Integer(2012), new Integer(2013),
                new Integer(2014), new Integer(2015), new Integer(2016), new Integer(2017), new Integer(2018),
                new Integer(2019), new Integer(2020), new Integer(2021), new Integer(2022), new Integer(2023));
        selectYear.setPromptText("Select Year");

        selectMonth.getStyleClass().add("combo-box");
        selectYear.getStyleClass().add("combo-box");

        hbox.getChildren().add(selectYear);
        hbox.getChildren().add(selectMonth);
        hbox.getChildren().add(submitbtn);
        hbox.setAlignment(Pos.CENTER);

        monthString = getMonthName(currentMonth.get(Calendar.MONTH));
        yearString = String.valueOf(currentMonth.get(Calendar.YEAR));

        Text tHeader = new Text();
        tHeader.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        tHeader.setText(monthString + "," + yearString);

        tHeader.getStyleClass().add("custom-text");
        mybox.setTop(hbox);
        hbox.getChildren().add(tHeader);

        hbox.setAlignment(Pos.TOP_CENTER);
        hbox.setPadding(new Insets(60, 100, 10, 70));

        submitbtn.setOnAction((event) -> {
            monthString = selectMonth.getValue();
            yearString = String.valueOf(selectYear.getValue());
            if (monthString == null || yearString == null) {
                alert = new Alert(AlertType.ERROR);
                alert.setTitle("Alert");
                alert.setHeaderText("Attention");
                alert.setContentText("Please enter a month and year");

                ButtonType okButton = new ButtonType("OK");

                alert.getButtonTypes().setAll(okButton);

                alert.showAndWait().ifPresent(response -> {
                    if (response == okButton) {
                        System.out.println("User clicked OK");
                    }
                });
            } else {
                hbox.getChildren().clear();
                gpBody.getChildren().clear();
                int monthNumber = getMonthNumber(monthString);
                int yearNumber = selectYear.getValue();
                // currentMonth.set(Calendar.MONTH, monthNumber);
                // currentMonth.set(Calendar.YEAR, selectYear.getValue());

                mybox.getChildren().clear();
                currentMonth = new GregorianCalendar(yearNumber, monthNumber, 1);
                // mybox = drawHeader(mybox, hbox);
                // mybox = drawBody(mybox);
                drawCalendar(mybox);
            }

        });
        return mybox;
    }

    private BorderPane drawBody(BorderPane mybox) {
        gpBody = new GridPane();
        gpBody.setMaxSize(1000, 500);

        for (int day = 1; day <= 7; day++) {
            // gpBody.getChildren().add(createButtonForDayName(getDayName(day)));
            Text tDayName = new Text(getDayName(day));
            tDayName.setFont(new Font(16));
            gpBody.add(tDayName, day - 1, 0);

        }

        int currentDay = currentMonth.get(Calendar.DAY_OF_MONTH); // 1
        int daysInMonth = currentMonth.getActualMaximum(Calendar.DAY_OF_MONTH); // 31
        int dayOfWeek = currentMonth.get(Calendar.DAY_OF_WEEK); // 4
        // days in current month
        int row = 1;
        for (int i = currentDay; i <= daysInMonth; i++) {
            if (dayOfWeek == 8) {
                dayOfWeek = 1;
                row++;
            }

            // gpBody.getChildren().add(createButtonForDayInMonth(String.valueOf(i)));
            gpBody.add(DairyCalendar.createButtonForDayInMonth(String.valueOf(i), currentMonth), dayOfWeek - 1, row);

            currentDay++;
            dayOfWeek++;
        }
        // Previous Month
        dayOfWeek = currentMonth.get(Calendar.DAY_OF_WEEK);
        if (currentDay != 1) {
            Calendar prevMonth = getPreviousMonth(currentMonth);
            int daysInPrevMonth = prevMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
            for (int i = dayOfWeek - 2; i >= 0; i--) {
                gpBody.add(createButtonForDayInPrevMonth(String.valueOf(daysInPrevMonth)), i, 1);
                daysInPrevMonth--;
            }
        }

        currentMonth.set(Calendar.DAY_OF_MONTH, currentMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
        dayOfWeek = currentMonth.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek != 7) {
            int day = 1;
            for (int i = dayOfWeek; i < 7; i++) {
                gpBody.add(createButtonForDayInNextMonth(String.valueOf(day)), i, row);
                day++;
            }
        }
        mybox.setCenter(gpBody);
        gpBody.setAlignment(Pos.CENTER);

        VBox vbox = new VBox();
        mybox.setLeft(vbox);
        vbox.prefWidth(300);
        vbox.prefHeight(500);

        Text msg = new Text("Hello , " + "\n" + "Have a wonderful day");
        Rotate rotate = new Rotate(-21, 0, 0);
        msg.getTransforms().add(rotate);
        vbox.getChildren().add(msg);
        vbox.setPadding(new Insets(00, 50, 50, 100));
        vbox.setAlignment(Pos.CENTER);

        msg.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        msg.getStyleClass().add("my-msg");
        return mybox;
    }

    public static String getMonthName(int n) {
        String[] monthNames = {
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "November", "December"
        };

        return monthNames[n];
    }

    private int getMonthNumber(String monthName) {
        int ans = 0;

        switch (monthName) {
            case "January":
                ans = 0;
                break;
            case "February":
                ans = 1;
                break;
            case "March":
                ans = 2;
                break;
            case "April":
                ans = 3;
                break;
            case "May":
                ans = 4;
                break;
            case "June":
                ans = 5;
                break;
            case "July":
                ans = 6;
                break;
            case "August":
                ans = 7;
                break;
            case "September":
                ans = 8;
                break;
            case "October":
                ans = 9;
                break;
            case "November":
                ans = 10;
                break;
            case "December":
                ans = 11;
                break;
            default:
                ans = Calendar.MONTH;
        }
        return ans;
    }

    private String getDayName(int n) {
        StringBuilder sb = new StringBuilder();
        switch (n) {
            case 1:
                sb.append("Sunday");
                break;
            case 2:
                sb.append("Monday");
                break;
            case 3:
                sb.append("Tuesday");
                break;
            case 4:
                sb.append("Wednesday");
                break;
            case 5:
                sb.append("Thursday");
                break;
            case 6:
                sb.append("Friday");
                break;
            case 7:
                sb.append("Saturday");
                break;
        }
        return sb.toString();
    }

    private GregorianCalendar getPreviousMonth(Calendar cal) {
        int cMonth = cal.get(Calendar.MONTH);
        int pMonth = cMonth == 0 ? 11 : cMonth - 1;
        int pYear = cMonth == 0 ? cal.get(Calendar.YEAR) - 1 : cal.get(Calendar.YEAR);
        return new GregorianCalendar(pYear, pMonth, 1);
    }

    /*
     * private Button createButtonForDayInMonth(String n) {
     * Button btn = new Button(n);
     * btn.setPrefSize(150, 100);
     * btn.getStyleClass().add("my-button");
     * 
     * btn.setOnAction((event) -> {
     * String dateValue = btn.getText();
     * String monthName = getMonthName(currentMonth.get(Calendar.MONTH));
     * String yearNumber = String.valueOf(currentMonth.get(Calendar.YEAR));
     * 
     * finalDate = dateValue + " " + monthName + " " + yearNumber;
     * Achivement obj = new Achivement();
     * Stage stage = new Stage();
     * try {
     * obj.start(stage);
     * } catch (Exception e) {
     * 
     * }
     * });
     * return btn;
     * }
     */
    private Button createButtonForDayInPrevMonth(String n) {
        Button btn = new Button(n);
        btn.setPrefSize(150, 100);
        btn.setTextFill(Color.GRAY);
        return btn;
    }

    private Button createButtonForDayInNextMonth(String n) {
        Button btn = new Button(n);
        btn.setPrefSize(150, 100);
        btn.setTextFill(Color.DARKGRAY);
        return btn;
    }

    public static String getFinalDate() {
        System.out.println(finalDate);
        return finalDate;
    }

}

public class DairyCalendar extends Application {

    static Stage stage1;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public static Button createButtonForDayInMonth(String n, Calendar currentMonth) {
        int same = 0;
        Calendar sample = new GregorianCalendar();
        int todayActualDate = sample.get(Calendar.DAY_OF_MONTH);
        int temp = Integer.parseInt(n);

        int todayActualMonthNumber = sample.get(Calendar.MONTH);
        String todayActualMonth = MyCalendar.getMonthName(todayActualMonthNumber);
        String tempMonth = MyCalendar.getMonthName(currentMonth.get(Calendar.MONTH));

        int todayActualYear = sample.get(Calendar.YEAR);
        int tempYear = currentMonth.get(Calendar.YEAR);
        if (todayActualDate == temp && todayActualMonth.equals(tempMonth) && todayActualYear == tempYear) {
            same = 0;
        } else {
            same = 1;
        }

        Button btn = new Button(n);
        btn.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        btn.setPrefSize(150, 100);
        btn.getStyleClass().add("my-button");

        if (same == 0) {
            btn.getStyleClass().add("today-button");
        } else {
            btn.getStyleClass().add("my-button");
        }

        btn.setOnAction((event) -> {
            String dateValue = btn.getText();
            String monthName = String.valueOf(currentMonth.get(Calendar.MONTH)+1);
            String yearNumber = String.valueOf(currentMonth.get(Calendar.YEAR));

            MyCalendar.finalDate= yearNumber+ "-"+ monthName +"-"+ dateValue;
            Achivement obj = new Achivement();
            // Stage stage = new Stage();
            try {
                obj.start(stage1);
            } catch (Exception e) {

            }
        });
        return btn;
    }

    public void start(Stage stage) throws Exception {

        stage1 = new Stage();
        stage1.setTitle("MemoryPalette");
        MyCalendar mycalendar = new MyCalendar();
        BorderPane borderpane = mycalendar.getMyBox();
        Scene scene = new Scene(borderpane);
       
        borderpane.getStyleClass().add("borderpane");
        scene.getStylesheets().add("style/style.css");
        stage1.setMaximized(true);

        stage1.setScene(scene);

        stage1.show();
    }
}
