import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class view extends Application {

    private static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/Diary";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "Warmachine@17";

    public void start(Stage stage) throws Exception {
        VBox root = new VBox(10);
        Scene scene = new Scene(root, 1000, 750);
        DatePicker datePicker = new DatePicker();
        Button happyButton = new Button("Happy");
        Button angryButton = new Button("Angry");
        Button dailyNotesButton = new Button("Daily Notes");
        TextArea textArea = new TextArea();
        textArea.setPrefWidth(400);
        textArea.setPrefHeight(300);

        // Handle Happy Button Click
        happyButton.setOnAction(event -> {
            String selectedDate = datePicker.getValue() != null ? datePicker.getValue().toString() : "";
            String mood = "happy";
            fetchAndDisplayData(selectedDate, mood, textArea);
        });

        // Handle Angry Button Click
        angryButton.setOnAction(event -> {
            String selectedDate = datePicker.getValue() != null ? datePicker.getValue().toString() : "";
            String mood = "angry";
            fetchAndDisplayData(selectedDate, mood, textArea);
        });

        // Handle Daily Notes Button Click
        dailyNotesButton.setOnAction(event -> {
            String selectedDate = datePicker.getValue() != null ? datePicker.getValue().toString() : "";
            String mood = "daily notes";
            fetchAndDisplayData(selectedDate, mood, textArea);
        });

        root.getChildren().addAll(datePicker, happyButton, angryButton, dailyNotesButton, textArea);
        stage.setScene(scene);
        stage.show();
    }

    private void fetchAndDisplayData(String date, String mood, TextArea textArea) {
        String query = "SELECT description FROM entry WHERE date = ? AND mood = ?";
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, date);
            preparedStatement.setString(2, mood);
            ResultSet resultSet = preparedStatement.executeQuery();
            StringBuilder resultText = new StringBuilder();
            while (resultSet.next()) {
                String description = resultSet.getString("description");
                resultText.append(description).append("\n");
            }
            textArea.setText(resultText.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            textArea.setText("Error fetching data.");
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
