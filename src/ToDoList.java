import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.*;
//import java.util.ArrayList;
//import java.util.List;

public class ToDoList extends Application {

    private VBox taskList = new VBox(10);
    private TextField inputField;

    @Override
    public void start(Stage primaryStage)throws Exception{
        inputField = new TextField();
        inputField.setStyle("-fx-background-color: lightblue;-fx-background-radius: 15; -fx-text-fill: black; -fx-border-radius: 20;");

        HBox hb = new HBox();
        hb.setAlignment(Pos.CENTER);

        Button addButton = new Button("");
        addButton.setOnAction(event -> addTodoItem());

        Button backButton = new Button("Back");
        backButton.setOnAction(event -> backTodo());
        backButton.setOnAction(e->{
            Memory memory = new Memory();
            try{
            memory.start(primaryStage);
            }
            catch(Exception e1){

            }
        });
        
        backButton.setStyle("-fx-background-color: brown;-fx-background-radius: 10; -fx-text-fill: black; -fx-border-radius: 10;");
        backButton.setFont(Font.font("Stencil", 25));

        Image img = new Image("todolistImage/add.png");
        ImageView iV = new ImageView(img);
        iV.setFitHeight(40);
        iV.setFitWidth(55);

        addButton.setGraphic(iV);

        HBox inputLayout = new HBox(10, backButton, inputField, addButton);
        inputLayout.setPadding(new Insets(10));
        inputLayout.setAlignment(Pos.CENTER);
        HBox.setMargin(inputLayout, new Insets(100));

        VBox mainLayout = new VBox(20, taskList, inputLayout);
        mainLayout.setPadding(new Insets(20));
        mainLayout.setAlignment(Pos.CENTER);

        inputField.setFont(Font.font("Stencil", 30));

        hb.getChildren().addAll(inputLayout, mainLayout);

        Scene scene = new Scene(hb,2000,1000);
        primaryStage.setMaximized(true);

        

        //image 1
        Image image = new Image("todolistImage/notes.jpg");

        BackgroundImage backgroundImage = new BackgroundImage(image,
        BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
        BackgroundPosition.CENTER, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
        
        hb.setBackground(new Background(backgroundImage));

        /*primaryStage.setFullScreen(false);
        primaryStage.setHeight(750);
        primaryStage.setWidth(1350);*/

        // Load tasks when the application starts
        loadTasks();

        // Rest of your code...

        primaryStage.setTitle("To-Do List");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Object backTodo() {
        return null;
    }

    private void addTodoItem() {
        String itemText = inputField.getText().trim();

        if (!itemText.isEmpty()) {
            CheckBox taskItem = new CheckBox(itemText);
            taskItem.setFont(Font.font("Stencil", 30));

            Image img = new Image("todolistImage/trash.png");
            ImageView iV = new ImageView(img);
            iV.setFitHeight(35);
            iV.setFitWidth(35);

            Button deleteButton = new Button("", iV);

            HBox taskLayout = new HBox(10, taskItem, deleteButton);

            deleteButton.setOnAction(event -> {
                taskList.getChildren().remove(taskLayout);
                // Save tasks when a task is deleted
                saveTasks();
            });

            taskList.getChildren().add(taskLayout);
            inputField.clear();

            // Save tasks when a new task is added
            saveTasks();
        }
    }

    // Method to save tasks to a text file
    private void saveTasks() {
        try (PrintWriter writer = new PrintWriter("tasks.txt")) {
            for (Node node : taskList.getChildren()) {
                HBox hbox = (HBox) node;
                CheckBox checkBox = (CheckBox) hbox.getChildren().get(0);
                writer.println(checkBox.getText());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Method to load tasks from a text file
    private void loadTasks() {
        try (BufferedReader reader = new BufferedReader(new FileReader("tasks.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                addTodoItem(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addTodoItem(String itemText) {
        CheckBox taskItem = new CheckBox(itemText);
        taskItem.setFont(Font.font("Stencil", 30));
    
        Image img = new Image("todolistImage/trash.png");
        ImageView iV = new ImageView(img);
        iV.setFitHeight(35);
        iV.setFitWidth(35);
    
        Button deleteButton = new Button("", iV);
    
        HBox taskLayout = new HBox(10, taskItem, deleteButton);
    
        deleteButton.setOnAction(event -> {
            taskList.getChildren().remove(taskLayout);
            saveTasks();
        });
    
        taskList.getChildren().add(taskLayout);
    }
    
    public static void main(String[] args)throws Exception {
        launch(args);
    }
}
