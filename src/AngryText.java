import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class AngryText extends Application {

    @Override
    public void start(Stage primaryStage) {
        Image image = new Image("CalenderPageImage/angIcon.png");
        

        
        ImageView imageView = new ImageView(image);
       
        imageView.setFitWidth(350); 
        imageView.setFitHeight(350);
        imageView.setStyle("-fx-border-radius:100;");

        Text title = new Text("Angry");
        title.setStyle("-fx-font-size: 80px;-fx-font-family: 'Lucida Handwriting';"); 
        title.setFont(Font.font("Verdana", FontWeight.BOLD, 24)); 
        title.setFill(javafx.scene.paint.Color.rgb(216, 0, 50)); 

       

        
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);

        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHgrow(Priority.ALWAYS); 
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHalignment(HPos.RIGHT); 

        
        gridPane.getColumnConstraints().addAll(column1, column2);

    
        GridPane.setHalignment(title, HPos.CENTER);
        gridPane.add(title, 0, 0);
        gridPane.setMargin(title,new Insets(0, 0, 0,200));
        
        TextArea userInput = new TextArea();
        gridPane.setMargin(userInput,new Insets(0, 0, 10,150));
        userInput.setPromptText("Share Angry Moments");
        userInput.setFont(Font.font("Arial", 45)); // Set the font and size
        userInput.setStyle("-fx-pref-height: 500px; -fx-background-color: transparent; -fx-border-color:LIGHTBLUE; -fx-border-width: 2px;-fx-border-radius:10px;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);");
       
        DatePicker date=new DatePicker();
        GridPane.setHalignment(date,HPos.CENTER);
        gridPane.add(date,1,1);
        
        
        Button back=new Button("Back");
        Button submit=new Button("Submit");
        back.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 25));
        back.setStyle("-fx-background-color: #952323; -fx-text-fill: white; -fx-padding: 10 20; -fx-background-radius: 10;");


        submit.setFont(Font.font("Monotype Corsiva", FontWeight.NORMAL, 25));
        submit.setStyle("-fx-background-color: #27ae60; -fx-text-fill: white; -fx-padding: 10 20; -fx-background-radius: 10;");

       
        gridPane.setMargin(back,new Insets(35, 0, 0,150));
        gridPane.setMargin(submit,new Insets(35, 0, 0,0));

        back.setOnAction(event -> {
            Memory memory = new Memory();
            try{
                memory.start(primaryStage);
            }
            catch(Exception e1){
                
            }
        });
            

        GridPane.setHalignment(back,HPos.LEFT);
        GridPane.setHalignment(submit,HPos.RIGHT);
        gridPane.add(back,0,3);
        gridPane.add(submit,0,3);



        GridPane.setHalignment(userInput, HPos.CENTER);

    
        gridPane.add(userInput, 0, 1);

        
        gridPane.add(imageView, 1, 0);

        
        Scene scene = new Scene(gridPane, 2000, 1000);
        primaryStage.setScene(scene);
        
       gridPane.setStyle(
            "-fx-background-image: url(InsertPageImage/page.jpg); " +
            "-fx-background-size: cover; " +
            "-fx-background-repeat: no-repeat; " +
            "-fx-background-position: center center;"
    );
        String url = "jdbc:mysql://127.0.0.1:3306/Diary";
        String username = "root";
        String password = "Warmachine@17";

        submit.setOnAction(event -> {
            String inputString = userInput.getText();
            String selectedDate = date.getValue() != null ? date.getValue().toString() : "No date selected";

            try {
                Connection connection = DriverManager.getConnection(url, username, password);
                String sql = "INSERT INTO entry (date, id, description) VALUES (?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, selectedDate);
                preparedStatement.setInt(2, 2); 
                preparedStatement.setString(3, inputString);
                preparedStatement.executeUpdate();
                connection.close();

                System.out.println("Data stored in the database.");
                userInput.clear();
                date.getEditor().clear();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    

        primaryStage.setTitle("Memory Palette");
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
